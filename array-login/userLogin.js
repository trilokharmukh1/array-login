let peopleData = require('./peopleData');

//find all people based on gender
function findPeople(gender, userData) {
    let result = peopleData.filter((people) => {
        return people.gender == gender;
    })

    return result;
}


// this function split IP address into their components
function splitIpAddress(peopleData) {
    let result = peopleData.map((people) => {
        ipAddress = people.ip_address.split('.');

        return ipAddress.map((ipElement) => {
            return parseInt(ipElement)
        })

    })

    return result

}



// sum of all the second components of the ip address
function sumSecondComponentsOfIpAddress(peopleData, index) {
    let result = peopleData.reduce((sum, people) => {
        ipAddress = people.ip_address.split('.');
        sum += parseInt(ipAddress[index - 1]);
        return sum;
    }, 0)

    return result;
}
// console.log(sumSecondComponentsOfIpAddress(peopleData,4));


// full name of each person and store it in a new key 
function addFullName(peopleData) {
    let result = peopleData.map((people) => {
        let fullName = people.first_name + " " + people.last_name;
        people["full_name"] = fullName
        return people;
    })

    return result;

}


// Filter out all the specific domain name email id
function findSpecificDomainEmailId(peopleData, domainName) {
    let result = peopleData.filter((people) => {
        let subEmail = people.email.slice(people.email.length - domainName.length);
        if (subEmail == domainName) {
            return people;
        }

    })
    return result;
}


//Sort the data in descending order of first name
function sortName(peopleData){
    return peopleData.sort((first, second)=>{
        return second.first_name.localeCompare(first.first_name);
    })
}
